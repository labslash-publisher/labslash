﻿---
title: "Sécurisation d'un VPS - Partie 3 - Mots de passe et astuces"
date: 2020-07-06T15:54:05+02:00
draft: false
author: Pr.Tourneslash
---

Voici le dernier volet sur la sécurisation d'un petit VPS privé, dans cette troisième partie nous allons particulièrement nous concentrer sur le choix des mots de passe. Cet article pourra donc être utile pour d'autres choses que votre VPS.

## Utilisation de mots de passe forts

Nous en avions parlé brièvement dans le dernier article, il est important, pour que tout ce que nous ayons fait précédemment soit utile, de s'assurer d'avoir des mots de passe fort. Alors je vous propose de discuter un peu mot de passe et bonne pratique.

Les mots de passe forts, on en entend parler de plus en plus souvent, et ça devient obligatoire un peu partout. Difficile aujourd'hui de s'inscrire sur un site sans être obligé de fournir un lot de conditions satisfaisant des règles de création de mot de passe, mais à quoi ça sert ?

Vous vous souvenez de l'attaque brute force ? On en a discuté dans l'article précédent, et grossièrement on sait maintenant qu'avec un ordinateur on est capable de tester très rapidement des milliers de mots de passe sur un service, pour pouvoir trouver le bon. Les mots de passe forts viennent compliquer cette technique, car l'utilisation des caractères spéciaux, majuscule et nombre augmente le nombre de combinaison possible. Enfin, ça, c'était l'idée derrière la publication du [NIST](https://fr.wikipedia.org/wiki/National_Institute_of_Standards_and_Technology) n°[800-63](https://pages.nist.gov/800-63-3/) qui proposait un ensemble de règles autour de l'authentification, et particulièrement à travers son appendice A.

Depuis que cet ensemble de propositions a été publié, nombre de sites et services ont décidé d'appliquer tout particulièrement ces directives parmi celles proposées dans le document. Cependant, des études ont tenté de déterminer si ces règles avaient une véritable importance. À titre d'exemple, [l'étude](http://www.cs.umd.edu/~jkatz/security/downloads/passwords_revealed-weir.pdf) proposée par Matt Weir, Sudhir Aggarwal, Michael Collins et Henry Stern conclue de la manière suivante :

> " Our findings were that absent an external password creation policy where the system manually adds randomness to a user’s password, or an implicit policy where a reject function disallows weak passwords, most common password creation policies remains vulnerable to online attack."

Comprenez :

> " Nos découvertes ont été que mise à pars ceux avec une norme de création de mots de passe, ou le système ajoute manuellement de l'aléatoire au mot de passe de l'utilisateur, ou d'une norme implicite ne permettant pas l'utilisation de mots de passe faible, la majorité des règles de création de mots de passe restent faibles "

En fait, les résultats de cette étude montrent que même en ajoutant des caractères spéciaux ou des numéros dans le mot de passe, il est toujours possible de les déterminer, puisque le comportement humain est prévisible.

Leur conclusion est donc qu'il est nécessaire d'avoir une partie d'aléatoire dans un mot de passe, ou que les règles de création de mots de passe ne soient pas explicites.

En effet, si les règles de création de mots de passe sont explicites, alors il n'est pas difficile pour l'attaquant d'adapter  son algorithme.  Par exemple, si je souhaite renforcer la sécurité des mots de passe sur un de mes services, et que j'annonce explicitement que celui-ci doit contenir au moins un caractère spécial, cela va permettre à l'attaquant d'éliminer de son dictionnaire tous ceux n'en contenant pas. Finalement, on ne renforce pas tant que ça la sécurité.

> Ok, c'est  mignon tout ça, mais du coup je fais comment moi pour que mon mot de passe soit sécurisé ?

Et bien comme l'on dit les chercheurs de l'étude, rajouter un max d'aléatoire. Avec ça, difficile pour un algorithme qui se base sur des statistiques de déterminer le mot de passe. Bien sûr, il est aussi conseillé d'avoir un mot de passe pas trop cours ( + de 7 caractères). Si vous respectez ces deux règles dans le choix de vos mots de passe, vous réduisez considérablement les chances que l'on découvre votre mot de passe.

L'aléatoire, ce n’est pas simple à retenir, alors plusieurs choix s'offrent à vous. Vous pouvez utiliser un gestionnaire de mot de passe sécurisé (on vous en présentera surement dans notre section outils), ou alors générer une passphrase, qui est ensemble de mots aléatoires, mais plus faciles a retenir, car ce sont de vrais mots, vous pouvez aller [ici](https://www.rempe.us/diceware/#french) pour générer une passphrase aléatoirement.

## Ne pas recycler son mot de passe

La deuxième règle, très importante lorsqu'il s'agit des mots de passe, c'est de ne jamais les recycler, en effet, si vous utiliser le même mot de passe d'accès pour votre VPS et sur un site internet, que celui-ci se fait dérober ça  liste d'identifiants, votre mot de passe risque ensuite de figurer dans les dictionnaires de mot de passe qui pourrons potentiellement être utilisé pour brute force votre VPS. Vous pouvez d'ailleurs constater si vos identifiants ont déjà figuré dans des fuites de données provenant de service en ligne grâce au site ["have i been pwned ?" ](https://haveibeenpwned.com/).

Bon, on récapitule, pour que notre VPS soit sécurisé, il faut donc que je lui fournisse un mot de passe assez long et aléatoire. Et surtout, il faut que celui-ci soit unique.

## Utiliser le service SSH sans mots de passe - L'authentification par paire de clés

Bon maintenant, on est renseigné niveau mot de passe, mais même avec nos astuces, c'est parfois difficile de le retenir. Alors voici une méthode vous permettant d'autoriser un accès SSH sans mot de passe à votre serveur depuis une machine en qui vous avez confiance. Attention, lorsque je parle de machines de confiance, je parle d'une machine personnelle sur laquelle seuls vous avez accès à votre session.

### Principe de fonctionnement

Sans rentrer dans le fonctionnement du protocole SSH qui mérite un article à part entière, je vous propose de comprendre comment fonctionne l'identification SSH à l'aide d'une paire de clés.

Pour comprendre le fonctionnement, expliquons rapidement le fonctionnement du chiffrement par clés asymétriques, qui est la méthode utilisée ici.

#### Chiffrement par clés asymétriques

Pour utiliser le chiffrement par clés asymétriques, utilisé par différents algorithmes, il est important de générer deux clés. La clé privée et la clé publique. La clé publique pourra chiffrer des messages que seule la clé privée peut lire. C'est la seule relation existante entre ces deux clés. La clé publique ne peut pas déchiffrer son propre message ni ceux chiffrés avec la clé privées. Vous l'aurez compris, comme son nom l'indique, la clé privée doit rester secrète, alors que la clé publique peut être divulguée.

#### Utilisation pour l'identification SSH

Dans le cas de notre protocole SSH, on va créer une paire de clés, sur la machine de confiance, on stockera de manière sécurisée la clé privée, et on partagera la clé publique correspondante à notre serveur en lui précisant que c'est une clé de confiance.

Voici comment fonctionnera ensuite le protocole SSH pour vérifier l'authentification par clé lors de la demande de connexion :

1. Le serveur envoie un message chiffré avec la clé publique au client

2. Le client déchiffre le message avec la clé privée et le renvoi au serveur

3. Le serveur reçoit le message déchiffré, il sait donc que le client détient la clé privée

4. Le serveur fait confiance au client et lui autorise l'accès

### Mise en place dans notre cas

Voici comment mettre cela en place dans notre cas, avec un client sous Linux.

Tout d'abord il faut créer une paire de clés avec l'algorithme que l'on souhaite utiliser, on en discutera certainement un jour, mais pour l'instant je vous propose d'utiliser des clés **Ed25519**. Pour ce faire rien de plus simple :

```
ssh-keygen -t ed25519
```

Il vous sera ensuite demandé la chose suivante :

```
Enter file in which to save the key (/home/myuser/.ssh/id_rsa):
```

Laissez vide, le répertoire de stockage par défaut nous convient très bien.

Puis on vous demande une passphrase. La passphrase protège l'accès à votre clé privée, si vous avez confiance à votre machine, vous n'êtes pas obligé d'en préciser une. Si vous en fournissez une, à chaque connexion SSH vous sera demandée la passphrase de votre clé ssh.

Voilà, votre clé est créée, il faut maintenant la fournir à notre VPS, pour cela on utilise l'outil `ssh-copy-id` qui copiera la clé dans le bon répertoire du serveur distant :

```
ssh-copy-id utilisateur@addresse_de_notre_serveur
```

Félicitations, vous disposez à présent d'un accès à votre serveur SSH  en authentification par clé depuis votre machine !

À travers cet article, nous avons pu comprendre exactement comment les mots de passe doivent être choisis pour nous protéger, et cela vient compléter et terminer notre série sur la sécurisation de nos serveurs VPS.

Gardez ces informations dans un coin de votre tête,

À la prochaine et commencez à expérimenter !
