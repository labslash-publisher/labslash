﻿---
title: "Sécurisation d'un VPS - Partie 1 - Le pare-feu"
date: 2020-07-01T21:40:45+02:00
draft: false
author: "Pr.Tourneslash"
---

Bienvenue dans cette suite de post qui relate mon expérience face à la configuration de la sécurité sur un serveur Debian. Cette série de post concerne les expérimentateurs ayant des connaissances de base sur le fonctionnement d'un système Linux.

N'étant pas très partisan de Google et de sa manière de gérer mes données, j'administre, depuis un long moment maintenant, un serveur permettant de remplacer certains de ses outils. Mes débuts furent longs et laborieux sur un serveur personnel hébergé chez moi derrière une faible connexion ADSL. Après quelques années à administrer ce serveur et une augmentation de mes besoins (l'arrivée de la fibre chez moi n'étant pas dans les prévisions), j'ai décidé de me pencher vers un hébergeur pour pouvoir accéder à mes services avec beaucoup plus d'efficacité.

Alors bien sûr, sortir ses données de chez google pour les donner à un hébergeur ça n’est pas d’une intelligence incroyable me diriez-vous. Il est vrai que cela demande réflexion. Mais rassurez-vous, je ne me suis pas tourné vers un autre « datavore » pour y stocker mes données. Une condition impérative de mon point de vue était que l’hébergeur soit en France, car le pays a l’avantage de proposer plus de lois permettant de protéger nos précieuses informations par rapport à d’autres.

Quand on dit France et hébergement, on pense à OVH. Cet hébergeur fait d'ailleurs cheval de bataille de sa localisation et donc de sa manière de traiter nos données. Cependant, biais cognitif ou réalité, j'ai tendance à fuir les structures importantes, car elles ne m'attirent pas confiance. Je n'ai donc chez eux que mes noms de domaines, car je souhaitais profiter de la puissance de leurs DNS. On m'a finalement fait découvrir Pulseheberg, hébergeur français beaucoup plus petit, mais très réactif pour des services de particulier. Il a la particularité d'être un hébergeur historiquement associatif ce qui en fait, à mon sens, un acteur beaucoup plus désintéressé par l'exploitation de nos données.

Convaincu par cet hébergeur (et les tarifs épousant parfaitement mes finances :wink: ) j'ai donc fini par louer un VPS. Un VPS, qui signifie « Virtual Private Serveur » est une machine virtuelle qui est mise à votre disposition. Techniquement, cela signifie que l'hébergeur détient chez lui des serveurs qui hébergent plusieurs machines virtuelles, qui sont administrées par ce que l'on appelle un hyperviseur.

N'ayant pas de gros besoins en puissance et une capacité financière réduite (Professeur n'est qu'un pseudo :sweat_smile: ) voici les caractéristiques du VPS :

- 2 Cœurs virtuels a 2,40Ghz
- 2 Go de RAM DDR3
- 30 Go en  RAID 10
- 100 Mbps

Le serveur est situé à Nanterre, en France, ce qui respecte mon cahier des charges :relieved:.

Une fois mon VPS mis en place, ce qui prend quelques heures, j'ai enfin accès à ma machine Debian (d'autres OS sont disponibles par défaut). C'est là qu'intervient la sécurisation de la machine.
En effet, celle-ci propose par défaut un accès à travers le protocole de communication SSH (secure shell). Si on ne sécurise pas rapidement le serveur, celui-ci risque de subir des attaques de type bruteforce sur le service SSH, afin d'essayer de gagner un accès à la machine.

La première chose à comprendre lorsqu'il s'agit de sécurité, c'est le fonctionnement des communications sur internet. Lorsque l'on prend un serveur chez un hébergeur, celui-ci est disponible frontalement sur internet. C'est-à-dire que l'IP qui lui est attribuée est une IP publique, accessible donc depuis n'importe quelles machines connectées à internet. Le serveur dispose ensuite de ports, ce sont des numéros qui permettent d'identifier à quelle application nous souhaitons nous adresser lorsque l'on échange avec les serveurs. Cela permet, par exemple, de choisir si l'on veut accéder à un site web hébergé sur le serveur ou au service SSH.

C'est là que nous allons commencer à discuter de pare-feu. Lorsque l'on dispose d'une machine qui est accessible depuis internet, le pare-feu devient une obligation. À quoi sert-il ? Et bien simplement, il permet d'interdire une connexion entrante/sortante/les deux, sur un ou plusieurs ports de notre machine. Ainsi, si une application écoute sur un port et que ce port est bloqué par le pare-feu, alors une connexion initiée sur ce port elle sera rejetée. Lorsque l'on découvre le fonctionnement du pare-feu, on peut légitimement se demander :

> À quoi ça sert de bloquer le port de mon application, si justement elle en a besoin pour fonctionner ?

Et bien pour deux raisons principales. La première étant que nous sommes humains et pas tous experts en administration système, donc il peut arriver que l'on installe une application, ou qu'une application soit installée à notre insu et écoute sur un port (car elle est déjà présente dans l'OS, s'installe comme une dépendance d'un autre paquet, ou simplement parce que l'on a pas lu la documentation...).
La seconde raison est que parfois, nous sommes conscients de l'installation d'une application proposant un service sur un port, mais que nous n'avons pas besoin que ce port soit accessible depuis l'extérieur, car nous souhaitons seulement l'utiliser en local (nous aurons l'occasion d'en reparler à travers d'autres sujets). Le pare-feu permet donc de bloquer l'accès au service tout en autorisant un accès local à l'application.

On comprend donc que le pare-feu est incontournable pour sécuriser notre VPS. Par défaut Debian (et une bonne partie des distributions Linux) intègre un outil, appelé IPtables, permettant de gérer les règles de connexion et donc de jouer le rôle de pare-feu. Il est très complet et permet de mettre en place des règles très précises, mais sa syntaxe est très lourde et laborieuse à utiliser pour un néophyte. Je me suis donc tourné vers un outil nommé UFW. C'est une surcouche logicielle d'IPtables qui permet d'appliquer des règles IPtables sans avoir à connaître sa syntaxe. Cependant, il ne permet que d'appliquer des règles simples sur les ports, c'est-à-dire interdire ou autoriser l'accès sur un port donné, éventuellement depuis une source particulière.

Mise en place et utilisation d'UFW :

UFW est présent dans les dépôts officiels de Debian, nous allons donc simplement l’installer à travers apt.


    $ sudo apt install ufw


Une fois UFW installé, celui-ci fonctionne comme service au sein de Debian. Par défaut, UFW bloque tous les ports de votre machine. Il est donc important de ne pas l'activer tout de suite, car votre connexion SSH serait alors bloquée par UFW. Si cela arrivait, pas de panique, souvenez-vous que nous avons un accès, via une console VNC, sur l'interface web de Pulsheberg.

Nous allons ensuite configurer UFW pour qu'il accepte les connexions sur le port lié au SSH afin de conserver l'accès à ce service, qui est utile pour configurer notre serveur à distance. Nous allons donc commencer par autoriser le port concerné (par défaut le port 22) dans UFW :


    $ sudo ufw allow 22

Avec cette commande nous autorisons le port 22, à recevoir des connexions de n'importe quelle provenance et de n'importe quel type de protocoles de transport.
Si vous souhaitez configurer UFW pour qu'il n'autorise qu'un protocole de transport sur un port (dans notre cas SSH ne fonctionne qu'en tcp), il est possible d'utiliser la commande suivante :


    $ sudo ufw allow 22/tcp


On peut noter qu'il est également possible d'indiquer à UFW un service au lieux d'un port. Dans ce cas, c'est le numéro de port standard attribué au service qui va être utilisé pour appliquer la règle. De ce fait la commande précédente aura le même effet que celle-ci :


    $ sudo ufw allow ssh


Cela peut avoir un intérêt car ce sera le nom du service qui apparaitra dans le statut d'UFW, ce qui peut aider à se souvenir de qui correspond à quoi. Toutefois si vous avez configuré votre service avec un port non-standard (ce qui peut avoir des avantages, mais ce n'est pas l'objet ici), la communication sera impossible en utilisant cette syntaxe.

Si vous souhaitez configurer UFW pour qu’il autorise seulement les connexions en provenance d’une certaine adresse IP, il est possible d’utiliser la commande suivante :


    $ sudo ufw allow from <adresse-ip>


---

***Attention***

Il est à noter que l’on autorise les connexions entrantes de cette IP sur tous les ports de notre machine.

---

À présent nous pouvons activer UFW sans risque que notre serveur nous claque la porte au nez :

    $ sudo ufw enable

Cette commande démarre le service et le configure pour qu'il se lance automatiquement à chaque démarrage.
Vous l'aurez compris, une fois ceci fait, le seul port qui acceptera des connexions sera le port 22, qui est utilisé par notre serveur SSH. Si vous veniez à installer un autre service nécessitant une connexion depuis l'extérieur, il vous faudra donc autoriser le port concerné, comme nous l'avons fait précédemment.

> OK, j'ai bien compris comment ouvrir un port, mais si j'en ai ouvert un que je veux refermer ?

Et bien si on veut refermer un port, voici comment s’y prendre. Tout d’abord, on liste les règles en les numérotant :

    $ sudo ufw status numbered

et nous obtenons un résultat de ce genre :

    Status: active
        To                         Action      From
        --                         ------      ----
    [ 1] 22/tcp                     ALLOW IN    Anywhere                  
    [ 2] 22/tcp (v6)                ALLOW IN    Anywhere (v6)

On remarque bien que le statut de UFW est actif, donc qu'il est en cours de fonctionnement. On peut ensuite visualiser nos règles. Ici nous avons autorisé seulement le port 22, comme prévu. Cependant, on remarque qu'il apparaît deux fois, pour deux protocoles réseau : IP et IPv6 (nous reparlerons de ses protocoles dans un autre post).
Bien entendu, si nous souhaitons fermer le port 22, nous allons retirer nos règles sur les deux versions du protocole IP. Pour ce faire, on retient le numéro de l'une des deux règles, puis on entre la commande suivante :

    $ sudo ufw delete <numéro de la règle>

Et on répète ces deux opérations pour supprimer la règle de l'autre version du protocole IP.
Puis comme à chaque modifications d'UFW, on recharge le service :

    $ sudo ufw reload

---

***Attention***

Cette partie était à titre d’exemple, si vous supprimez l’accès au port 22 vous perdrez votre accès SSH.

---

Il existe un grand nombre d'autres commandes bien plus poussées disponibles pour UFW mais pour cela nous vous invitons à allez lire une [documentation](https://doc.ubuntu-fr.org/ufw).

Vous savez à présent configurer un part-feu pour n'autoriser que les ports désirés à être accessible sur internet. Cependant, si vous vous souvenez encore un peu du début de cette lecture éprouvante, vous êtes certainement en train de vous poser une question (ou pas du tout parce que vous n'êtes pas forcément la pour ça :wink: ).

> Mais le part-feu il ne peut pas empêcher les attaques brutforce puisqu'il autorise l'accès au service SSH depuis internet ?

Effectivement, le part-feu permet de contrôler facilement l'accès aux ports du serveur, mais ne permet pas de se protéger des attaques sur les services autorisés. Il va donc falloir implémenter un autre outil pour se protéger de celles-ci. Mais ma longue rédaction approximative commençant à vous ennuyer, je vous propose que l'on étudie cela dans le prochain post.

Alors, à la prochaine et commencez à expérimenter !
