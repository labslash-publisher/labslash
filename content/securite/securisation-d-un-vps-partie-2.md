﻿---
title: "Sécurisation d'un VPS - Partie 2 - Fail2Ban"
date: 2020-07-02T13:20:33+02:00
draft: false
author: Pr.Tourneslash
---

Bienvenue dans la seconde partie de cette suite de posts sur la sécurisation de son VPS, si vous n'avez pas lu la première partie, on se retrouve [ici!](../securisation-d-un-vps-partie-1/)

Si vous avez la mémoire courte, ou si vous n'êtes pas revenus dans le coin depuis un petit bout de temps, je vous rafraîchis rapidement la mémoire. Dans la dernière partie, après avoir longuement monologué sur le choix de mon VPS, nous avions abordé la problématique de la sécurité en commençant par la mise en place d'un part-feu sur celui-ci. Cependant, nous avions constaté que le part-feu nous permettait seulement de nous protéger contre l'ouverture de port indésirable sur notre serveur, cependant il ne protège pas les ports ouverts contre les attaques. Nous allons donc tout au long de ce post, mettre en place un outil pour se protéger des attaques bruteforce.

> Oui mais dit donc, ça fait plusieurs fois que tu nous parles d'attaques bruteforce, ça te dérangerait de nous expliquer se que c'est ?

À oui, pardon, effectivement nous devrions commencer par définir se type d'attaque.

---

***Définition***
: L'attaque bruteforce, autrement dit attaque par force brute, est une méthode permettant de retrouver des identifiants de connexion (couple identifiant/mot de passe, clé, etc.) à un service. La méthode consiste à tester une à une  toutes les combinaisons possibles jusqu'à obtenir la bonne.

---

Alors, dit comme ça, si l’on est bon en math, on fait rapidement le calcul et on peut se dire :

> Mais attend, toutes les combinaisons possibles du clavier pour un couple d'identifiants, ça fait quand même un sacré paquet de tests avant de trouver le bon, non ?

Alors oui, si on commence à se palucher ça au clavier, on n’est pas sorti du data-center (je remets les expressions au goût du jour, ce n’est pas gagné :grimacing:). Mais aujourd'hui avec un script et un ordinateur on peut faire un nombre important de requêtes à la minute. Donc même s’il y a un très grand nombre de possibilités, ça ne coûte rien à l'attaquant de laisser sa machine tourner plusieurs jours jusqu'à se qu'il trouve nos identifiants.

Du coup, c'est un peu embêtant parce que ça veut dire qu'en quelques semaines, aux pires quelques jours, l'attaquant aura obtenu un accès à notre service et pourra donc faire ce qu'il veut de notre VPS.

> Et tiens d'ailleurs, pourquoi il fait ça lui ?! Je ne suis pas une banque, il ne gagne rien à avoir un accès à mon VPS, il n'y a rien avec de la valeur dessus de toute manière !

La majorité des attaques de ce type sont effectués par des robots qui scrutent internet à la recherche de serveurs vulnérables, et effectivement, la plupart du temps, ils n'ont que faire de ce qu'ils peuvent bien contenir (attention, cependant certains robots agissent à la manière de ransomware en chiffrant la totalité de vos données en attente d'une rançon). En fait, le robot cherche juste à avoir accès à votre serveur pour le zombifier, en faire un esclave, qui permettra à la personne physique derrière toute cette histoire, d'utiliser la puissance de votre serveur pour lancer d'autres attaques. Quand cette personne (plus couramment des organisations), obtient assez de machines zombies, elle peut concentrer leurs puissances et ainsi s'attaquer à des structures plus robustes, plus intéressantes, et permettant à l'attaquant de finalement mettre la main sur un butin :moneybag:.

Mais légalement, cela peut aller plus loin pour vous :

---

***Attention***

Vous êtes tenu pour responsable des actions effectué par votre VPS, si celui participe à votre insu à une attaque sur internet, vous serez la première personne contactée une fois votre VPS identifié.

---

Alors rassurez-vous, vous n'allez pas voir le RAID débarquer dans votre maison (à moins d'avoir un karma négatif :sweat_smile:), cependant vous risquer d'être contacté par votre hébergeur ou votre fournisseur d'accès internet vous avertissant de votre participation à une attaque et donc vous imposant de cesser toutes actions illégales sous peine de vous voir retirer l'accès au service.
Bien entendu si cela devait aller plus loin, les enquêteurs ne sont pas intéressés par les propriétaires de serveurs zombifiés et risquent de simplement vous ~~demander~~ imposer une collaboration, pour remonter à la source de l'attaque, après vous avoir passé le savon le plus effrayant de votre vie :smirk:. Pour s'informer un peu plus sur la question [le Code pénal](http://www.jurizine.net/2005/09/04/42-articles-323-1-a-323-7-du-code-penal-fraudes-informatiques) c'est pas mal :wink:!

Alors là, je vous ai peut-être un peu effrayé, mais c'est important d'être au courant des risques auxquels on s'expose sur internet. En tout cas maintenant vous en êtes convaincus, je l'espère, la sécurité c'est important, et il faut trouver un moyen pour se débarrasser de ces attaques bruteforce.

Heureusement, il y a quand même un paquet de gens qui ont besoin d'avoir des services en ligne et donc il existe évidemment des outils pour se protéger. Nous, on va s'intéresser à la référence des systèmes Linux lorsqu'il s'agit de se protéger contre le bruteforce. Son nom c'est Fail2Ban.

## Principe de fonctionnement

Avant de mettre en place Fail2Ban, on va essayer de comprendre comment ça marche. Parce que, vous le remarquerez au fur et à mesure, c'est toujours plus simple de mettre en place quelque chose quand on à compris comment ça marche.

Le principe est simple, mais il faut le connaître. En informatique, lorsque l'on fait les choses bien et qu'on développe un service, on propose ce que l'on appelle des logs. À travers ces logs on peut retracer exactement chaque action effectuée sur le service. Vous l'aurez compris, Fail2Ban se base sur les logs des services pour identifier les attaques bruteforce et ainsi bannir l'attaquant.

En effet, les services remontent normalement toutes les tentatives de connexion en précisant si elles ont été fructueuses ou non et en précisant l'adresse IP qui a initié cette connexion. Fail2Ban peut donc bannir toutes les IPs initiant des connexions erronées pour cause de mot de passe incorrect.

> Alors oui, c'est bien mignon tout ça, mais moi j'ai de gros doigts, et des fois sur mon clavier je me trompe dans le mot de passe. Du coup, je risque de me faire bannir et ça, c'est pas super ! :worried:

Pas d'inquiétude, Fail2Ban propose une granularité intéressante quand au nombre de connexion erronée avant de se faire bannir et aussi sur le temps de ban des adresses bannies. Il est par exemple possible de bannir une IP seulement après trois tentatives erronées et pour la durée que l'on veut (un jour, une semaine, un mois, etc.).

Concrètement, en réglant correctement les paramètres du ban, on peut ralentir considérablement le temps entre le début de l'attaque bruteforce et la détection des bons paramètres d'authentification. Transformant, par exemple, une attaque qui aurais pris quelques semaines, en une attaque prenant plusieurs années. Ce qui sécurise considérablement notre VPS car majorité des robots abandonnent l'attaque s’ils se font bannir longtemps. Cependant, il reste une petite probabilité, qu'avec un peu de persévérance quelqu'un finisse, un jour, par trouver votre mot de passe. C'est pour cela que je vous conseille d'actualiser celui-ci périodiquement, ce qui rend le brutefroce quasiment irréalisable (en respectant bien sûr les normes de mots de passe forts, ou même de [passphrase](https://fr.wikipedia.org/wiki/Phrase_secr%C3%A8te) si vous êtes perfectionniste).

## Configuration de Fail2Ban

OK, maintenant que nous avons compris comment ça fonctionne, voyons comment le mettre en place sur notre serveur.

Pour rappel, nous sommes sous Debian dans mon exemple, et le paquet Fail2Ban est disponible depuis les sources, donc pas de difficulté pour l'installer.

    $ sudo apt install fail2ban

Tout comme UFW précédemment, Fail2Ban se présente sous la forme d'un service au sein de notre système, cependant il est activé à l'installation, il est d'ailleurs possible de regarder l'état de ce service avec la commande suivante :

    $ sudo systemctl status fail2ban

Et on obtient une réponse de ce genre :

    ● fail2ban.service - Fail2Ban Service
    Loaded: loaded (/lib/systemd/system/fail2ban.service; enabled; vendor preset:
    Active: active (running) since Fri 2020-05-08 16:24:07 CEST; 1 months 24 days

Bon, c'est un extrait de réponse sur lequel on voit bien que Fail2Ban est activé, c'est bon signe pour nous !

La seconde étape est de faire un petit tour dans les fichiers de configurations de Fail2Ban. On va donc afficher les fichiers qui sont présents sous `/etc/fail2ban/` :

    $ ls /etc/fail2ban/

On obtient la liste suivante :

    action.d  fail2ban.conf  fail2ban.d  filter.d  jail.conf  jail.d paths-arch.conf
    paths-common.conf  paths-debian.conf  paths-opensuse.conf

Alors, faisons le tour des prérequis. Un nom finissant par *.d* est un répertoire, un nom finissant par *.conf* est un fichier de configuration, et un nom finissant par *.local* est un fichier de configuration qui écrase la configuration du fichier *.conf* correspondant.

> Très bien, merci pour ça, mais pourquoi on écrase une configuration avec un .local ?

Lors d'une mise à jour, les fichiers *.conf* peuvent être modifiés. Si vous avez effectué vous-même une modification sur un fichier, durant la mise à jour vous devrez résoudre un conflit pour choisir si vous souhaitez obtenir la nouvelle version ou garder votre version modifiée. Choix que beaucoup font sans en connaître les conséquences, et qui peut bousiller le fonctionnement de votre service.

C'est à ça que sers le *.local*, on a la garantie de récupérer les versions mises à jour des *.conf* sans avoir de conflits, et on sait que leurs configurations seront de toute manière écrasées par celles des *.local*, donc nos régles resterons pour de bon. On s'assure ainsi que notre service reste fonctionnel, mais que notre configuration est toujours présente et utilisée. Vous l'aurez compris :

---

**Important** : Afin d'éviter les conflits, on ne modifie que les fichiers en *.local* !

---

Maintenant que l'on connaît cette petite astuce, voyons à quoi servent les différents fichiers :

* Le fichier **fail2ban.conf** contient les informations relatives au fonctionnement du service Fail2Ban. C'est à dire, ses niveaux de logs, ses différents paramètres d’interaction avec le système, etc.
  La configuration par défaut nous convient, donc on ne touchera pas à ce fichier (en plus, c'est un *.conf*).

* Le fichier **jail.conf** contient une pléthore de préconfiguration pour mettre en place des ban(s) sur différents services. Un ban c'est quand on bannit quelqu'un d'un service, et le jail c'est la configuration déterminant si on doit bannir ou pas quelqu'un. C'est un *.conf*, on n'y touche pas !

* L'autre fichier qui nous intéresse est situé dans le dossier **jail.d** et s'intitule **defaults-debian.conf**, son chemin absolut est donc `/etc/fail2ban/jail.d/defaults-debian.conf`. Ce fichier a la particularité d'écraser le fichier *jail.conf*. Mais vous avez remarqué, c'est un *.conf* donc pas touche.

Alors voici comment je vous propose de configurer Fail2Ban pour que tout soit propre. Nous allons faire une copie de *jail.conf* vers **jail.local** dans lequel je vous propose de mettre nos paramètres généraux, c'est à dire, le nombre d'essais erronés maximum en un temps donné avant le ban, et le temps de ban. Ce fichier contiendra donc aussi par défaut des jails préconfigurer pour beaucoup de service, auxquels on ne touchera pas, mais sur lesquels on se basera.

On créera ensuite un fichier **defaults-debian.local** à côté du **defaults-debian.conf**. Dans ce fichier on placera toutes les modifications qui écraseront les paramètres de jails préconfigurés dans `jail.local`, ainsi, lorsque l'on veut regarder comment on a configuré un jail pour un service donné, il nous suffit de nous rendre dans le fichier `/etc/fail2ban/jail.d/defaults-debian.local`, et on s'assure que les mises à jour n'y changeront rien.

> Whouaaa, pas simple ton machin ! J'ai lu trois fois, et à part me coller une migraine, ça n'a pas fait grand-chose.

C'est vrai que c'est assez lourd comme méthode, mais rien ne vaut un petit exemple pour illustrer cette indigestion :wink:.

Dans un premier temps, on crée notre fichier **defaults-debian.local** et on fait notre copie *jail.conf* vers **jail.local**:

    $ sudo touch /etc/fail2ban/jail.d/defaults-debian.local
    $ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

Ensuite, allons configurer nos paramètres généraux dans **jail.local** (pour cela choisissez votre éditeur de fichier par défaut, ici pas d'élitisme vim, l'important c'est d'être à l'aise avec son outil :wink:) :

    $ sudo nano /etc/fail2ban/jail.local

 On cherche la catégorie **[DEFAULT]** (celle qui n'est pas commentée), sous laquelle on cherche les lignes suivantes en actualisant les valeurs :

    bantime = 168h
    maxretry = 5
    findtime = 10m

Ainsi on configure par défaut la possibilité à un utilisateur d'échouer cinq fois une connexion en 10 minutes avant de se faire bannir une semaine (168 heures). Vous pouvez modifier cette granularité comme ça vous arrange, mais restez cohérent.

On se rend ensuite dans la catégorie **[sshd]** du fichier qui correspond à la configuration du jail pour le serveur ssh (qui est pour l'instant le seul serveur que nous avons en place sur notre VPS dans mon exemple).
On remarque la composition suivante :

    [sshd]

    port    = ssh
    logpath = %(sshd_log)s
    backend = %(sshd_backend)s

Ici le jail est configuré, pour que le port soit "ssh", c'est à dire le port par défaut de se service. Le paramètre logpath définie ou allez chercher les logs de sshd, et contient une variable pointant vers les logs sshd par défaut sous Debian. Finalement backend permet de savoir quelles lignes du fichier de logs indiquent une connexion erronée et comment les traitées, une fois de plus la valeur est une variable préconfigurée pour Debian.

On peut enregistrer notre fichier et le quitter.

On se rend ensuite dans le fichier **defaults-debian.local** :

    $sudo nano /etc/fail2ban/jail.d/defaults-debian.local

 Dans ce fichier nous allons surcharger les paramètres du jail sshd qui ne nous conviennent pas. Par défaut, tout convient, cependant si vous avez changé de port SSH sur votre machine il faudra redéfinir le port, sinon il ne reste qu'à activer le jail :

    [sshd]
    port = <port personnalisé> #si changement de port ssh
    enabled = true

On quitte le fichier enregistré.

Il ne reste plus qu'a relancer le service Fail2Ban pour activer les modifications.

    $ sudo systemctl reload fail2ban

Et voilà, vous avez mis en place un jail fonctionnel sur votre service sshd. Vous pouvez réitérer ces actions pour d'autres services si nécessaire.

Pour finir, il peut être intéressant de vérifier l'état de nos jails afin de constater les adresses bannies. Dans un premier temps on peut vérifier les jails actifs avec la commande suivante :   

```
$ sudo fail2ban-client status
```

Ce qui nous permet donc d'en obtenir la liste :

```
Status
|- Number of jail:    1
`- Jail list:   sshd
```

Ici on peut constater que notre jail sshd est bien actif.

Si on souhaite vérifier les adresse bannies, on précise le jails sur lequel on souhaite obtenir l'état :

```
$ sudo fail2ban-client status sshd
```

Se qui nous retourne toutes les informations sur le jail :

```
Status for the jail: sshd
|- Filter
|  |- Currently failed:    7
|  |- Total failed:    615345
|  `- File list:    /var/log/auth.log
`- Actions
   |- Currently banned:    6
   |- Total banned:    97627
   `- Banned IP list:    104.218.55.91 59.56.99.130 161.35.101.169 218.92.0.204 103.253.146.142 194.152.206.12
```

Nous voila défendus contre le brutforce.

Après cette longue dissertation, je vous laisse à un peu de pratique !

Alors, à la prochaine et commencez à expérimenter !
